import {useState, useEffect} from 'react';

export enum Operations {
    START = 1, STOP, RESET, ADD5, SUBTRACT5
}

export const useTimer = () => {

    const [minutes, setMinutes] = useState<number>(25);
    const [seconds, setSeconds] = useState<number>(0);
    const [disabled, setDisabled] = useState<boolean>(false);

    const [status, setStatus] = useState<Operations>();

    useEffect(() => {
        let handle: any;
        if (status === Operations.START) {

            handle = setInterval(() => {
  
                setSeconds(prevSeconds => prevSeconds >= 59 ? 0 : prevSeconds + 1);

                if (seconds > 0 && seconds % 59 === 0) {
                    setMinutes(prevMin => prevMin >= 59 ? 0 : prevMin + 1);
                }

                // Stop the timer when mins and secs reach 00:00
                if (seconds >= 59 && minutes >= 59) {
                    clearInterval(handle);
                    setDisabled(true);
                    setStatus(Operations.STOP);
                }
            }, 1000);
        }

        if (status === Operations.RESET || status === Operations.STOP) {
            clearInterval(handle);
        }

        return () => clearInterval(handle);

    }, [status, seconds, minutes]);
    
    const doAction = (operation: Operations) => {

        const date = new Date();

        switch(operation) {
            case Operations.START: 
            case Operations.STOP:
                setStatus(operation);
                return;

            case Operations.RESET: 
               
                setSeconds(0);
                setMinutes(25);

                setDisabled(false);
                setStatus(operation);

                return;

            case Operations.ADD5: 
                
                setMinutes(prevMin => {
                    date.setMinutes(prevMin + 5);
                    return date.getMinutes();
                });
                return;

            case Operations.SUBTRACT5: 
                setMinutes(prevMin => {
                    date.setMinutes(prevMin - 5);
                    return date.getMinutes();
                });
                return;

            default:
                return;
        }
    }

    return {minutes, seconds, disabled, doAction};
}