import {renderHook, act} from '@testing-library/react-hooks';
import {useCountryFilter} from '../useCountryFilter';

describe('Test country filter hook', () => {

    it('Test all countries displayed when no country search is empty', () => {

        const { result } = renderHook(() => useCountryFilter());

        act(() => {
            result.current.displayAllCountriesList();
        });

        expect(result.current.selectedCountries.length).toBeGreaterThan(0);
    });
});