import {renderHook, act} from '@testing-library/react-hooks';
import {Operations, useTimer} from '../useTimer';

beforeEach(() => {
    jest.useFakeTimers();
});
afterEach(() => {
    jest.clearAllTimers();
})

describe('Test userTimer custom hook', () => {

    it('Test if relevant buttons are disabled when 00 : 00 is reached', () => {
        const {result} = renderHook(() => useTimer());

        act(() => {
            result.current.doAction(Operations.START);
            jest.advanceTimersByTime(2100000);
        })

        expect(result.current.disabled).toBeTruthy();
        expect(result.current.minutes).toBe(0);
        expect(result.current.seconds).toBe(0);
    });

})
