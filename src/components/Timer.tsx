import { useTimer, Operations } from '../behaviour/useTimer';

export const Timer = () => {

    const { 
        minutes,
        seconds,
        disabled,
        doAction } = useTimer();

    return (
        <>
            <div data-testid='time-section'>{minutes < 10 ? '0' + minutes : minutes} : {seconds < 10 ? '0' + seconds : seconds}</div>
            <button disabled={disabled} onClick = {(e) => doAction(Operations.START)} > START </button>
            <button disabled={disabled} onClick = {(e) => doAction(Operations.STOP)} > STOP </button>
            <button onClick = {(e) => doAction(Operations.RESET)} > RESET </button>
            <button onClick = {(e) => doAction(Operations.ADD5)} > +5 </button>
            <button onClick = {(e) => doAction(Operations.SUBTRACT5)} > -5 </button>
        </>
    );
}