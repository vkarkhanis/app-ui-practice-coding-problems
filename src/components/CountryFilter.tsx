import React from 'react';
import { useCountryFilter } from '../behaviour/useCountryFilter';

export const CountryFilter = () => {

    const {
        countries, 
        setSelectedCountries,
        selectedCountries, 
        displayAllCountriesList, 
        chosenCountry, 
        setChosenCountry} = useCountryFilter();

        const filterCountries = (e: React.ChangeEvent<HTMLInputElement>) => {

            const enteredValue = e && e.target.value ? e.target.value.toLowerCase() : "";
    
            setChosenCountry(enteredValue);
            setSelectedCountries(countries.filter((eachCountry: string) => 
                eachCountry.toLowerCase().includes(enteredValue)
            ));
            
        }

    return (
        <>
            <input type='text'
                onChange={filterCountries} 
                onFocus={(e: React.FocusEvent<HTMLInputElement>) => displayAllCountriesList()} 
                value={chosenCountry}
            />
            {selectedCountries.map((eachCountry: string, index: number) => {
                return <p data-testid={'country-list'} key={index} onClick={() => setChosenCountry(eachCountry)}>{eachCountry}</p>
            })}
        </>
    )
};