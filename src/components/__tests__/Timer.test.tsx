import {render, fireEvent, waitFor, act} from '@testing-library/react';
import {Timer} from '../Timer';

beforeEach(() => {
    jest.useFakeTimers();
});
afterEach(() => {
    jest.clearAllTimers();
})

describe('Test Timer Component', () => {

    it('check presence of components', () => {
        const {getByRole} = render(<Timer />);

        expect(getByRole('button', {name: /start/i})).toBeDefined();   
        expect(getByRole('button', {name: /stop/i})).toBeDefined();   
        expect(getByRole('button', {name: /reset/i})).toBeDefined();   
        expect(getByRole('button', {name: "+5"})).toBeDefined();   
        expect(getByRole('button', {name: "-5"})).toBeDefined();   
        
    });

});

describe('Test Timer Events', () => {

    it('Timer Start Test', async () => {
        const {getByTestId, getByText, getByRole} = render(<Timer />);

        expect(getByTestId('time-section')).toBeDefined();
        act(() => {
            fireEvent.click(getByText(/start/i));
            jest.advanceTimersByTime(1000);
        });

        
        await waitFor(() => expect(getByText('25 : 01')).toBeDefined());
        expect(getByRole('button', {name: /start/i})).toBeEnabled();   
        expect(getByRole('button', {name: /stop/i})).toBeEnabled();   
        expect(getByRole('button', {name: /reset/i})).toBeEnabled();   
        expect(getByRole('button', {name: "+5"})).toBeEnabled();   
        expect(getByRole('button', {name: "-5"})).toBeEnabled();   
    });

    it('Timer Stop Test', async () => {
        const {getByText, getByRole} = render(<Timer />);

        act(() => {
            fireEvent.click(getByText(/start/i));
            jest.advanceTimersByTime(1000);
        });
        
        await waitFor(() => expect(getByText('25 : 01')).toBeDefined());

        act(() => {
            fireEvent.click(getByText(/stop/i));
            jest.clearAllTimers();
       });

       await waitFor(() => expect(getByText('25 : 01')).toBeDefined());

       expect(getByRole('button', {name: /start/i})).toBeEnabled();   
       expect(getByRole('button', {name: /stop/i})).toBeEnabled();   
       expect(getByRole('button', {name: /reset/i})).toBeEnabled();   
       expect(getByRole('button', {name: "+5"})).toBeEnabled();   
       expect(getByRole('button', {name: "-5"})).toBeEnabled();
    });

    it('Timer Reset Test', async () => {
        const {getByText, getByRole} = render(<Timer />);

        act(() => {
            fireEvent.click(getByText(/start/i));
            jest.advanceTimersByTime(1000);
        });
        
        await waitFor(() => expect(getByText('25 : 01')).toBeDefined());

        act(() => {
            fireEvent.click(getByText(/reset/i));
            jest.clearAllTimers();
       });

       await waitFor(() => expect(getByText('25 : 00')).toBeDefined());

       expect(getByRole('button', {name: /start/i})).toBeEnabled();   
       expect(getByRole('button', {name: /stop/i})).toBeEnabled();   
       expect(getByRole('button', {name: /reset/i})).toBeEnabled();   
       expect(getByRole('button', {name: "+5"})).toBeEnabled();   
       expect(getByRole('button', {name: "-5"})).toBeEnabled();
    });

    it('Timer Add5 Test', async () => {
        const {getByText, getByRole} = render(<Timer />);

        act(() => {
            fireEvent.click(getByRole('button', {name: "+5"}));
        });
        
        await waitFor(() => expect(getByText('30 : 00')).toBeDefined());

        expect(getByRole('button', {name: /start/i})).toBeEnabled();   
        expect(getByRole('button', {name: /stop/i})).toBeEnabled();   
        expect(getByRole('button', {name: /reset/i})).toBeEnabled();   
        expect(getByRole('button', {name: "+5"})).toBeEnabled();   
        expect(getByRole('button', {name: "-5"})).toBeEnabled();
    });

    it('Timer Subtract5 Test', async () => {
        const {getByText, getByRole} = render(<Timer />);

        act(() => {
            fireEvent.click(getByRole('button', {name: "-5"}));
        });
        
        await waitFor(() => expect(getByText('20 : 00')).toBeDefined());

        expect(getByRole('button', {name: /start/i})).toBeEnabled();   
        expect(getByRole('button', {name: /stop/i})).toBeEnabled();   
        expect(getByRole('button', {name: /reset/i})).toBeEnabled();   
        expect(getByRole('button', {name: "+5"})).toBeEnabled();   
        expect(getByRole('button', {name: "-5"})).toBeEnabled();
    });

});