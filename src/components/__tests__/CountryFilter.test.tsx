import {render, fireEvent} from '@testing-library/react';
import { CountryFilter } from '../CountryFilter';

afterEach(() => {
    jest.clearAllTimers();
})

describe('Test component rendering', () => {
    it('Check component displayed appropriately', () => {
        const {getByRole} = render(<CountryFilter />);

        expect(getByRole('textbox')).toBeDefined();
    });
});

describe("Populate country list based on filters", () => {

    it("Populate all countries", async () => {
        const {getByRole, findAllByTestId} = render(<CountryFilter />);
        fireEvent.focus(getByRole('textbox'));

        expect((await findAllByTestId('country-list')).length).toBeGreaterThan(0);

    });

    it("Filter multiple Countries", async () => {
        const {getByRole, findAllByTestId} = render(<CountryFilter />);
        fireEvent.change(getByRole('textbox'), {target: {value: 'ca'}});
        expect((await findAllByTestId('country-list')).length).toBeGreaterThan(0);
    });

    it("Filter one Country", async () => {
        const {getByRole, findAllByTestId} = render(<CountryFilter />);
        fireEvent.change(getByRole('textbox'), {target: {value: 'anDoRra'}});
        expect((await findAllByTestId('country-list')).length).toBe(1);
    });

    it("No country returned", () => {
        const {getByRole, queryByTestId} = render(<CountryFilter />);
        fireEvent.change(getByRole('textbox'), {target: {value: 'XBsjfBjd'}});
        expect(queryByTestId('country-list')).toBe(null);
    });
});

describe("Country selection", () => {

    it("Select one country from populated list", async () => {

        const {getByRole, findAllByTestId} = render(<CountryFilter />);
        fireEvent.change(getByRole('textbox'), {target: {value: 'AndoRra'}});
        const countryList: Array<HTMLElement> = await findAllByTestId('country-list');

        expect(countryList.length).toBe(1);
        fireEvent.click(countryList[0]);
        expect(getByRole('textbox')).toHaveValue("Andorra");
        
    });
});