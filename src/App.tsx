import { CountryFilter } from './components/CountryFilter';
import './App.css';

function App() {
  return (
    <div className="App">
      <CountryFilter />
    </div>
  );
}

export default App;
